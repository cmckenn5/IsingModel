#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

def Ising_E(x,y):

    s = M[x,y] * (M[(x+1) % n,y] + M[x, (y+1) % n] + M[(x-1) % n, y] + M[x,(y-1) % n])
    E = -s
    return E

def metrop(*args): #Metropolis Algorithm

    x = np.random.randint(n)
    y = np.random.randint(n)
    i = Ising_E(x,y)
    M[x,y] *= -1
    f = Ising_E(x,y)
    deltaE = f - i
    if(np.random.uniform(0,1) > np.exp(-deltaE/T)):
        M[x,y] *= -1

    mesh.set_array(M.ravel())
    return mesh,

def init_spin_condition(opt):
#initial spin conditions
    if opt == 'h':
        #Hot Start/ Non Equilibrium Start
        M = np.random.randint(2, size=(n, n)) #nxn Lattice with random spin configuration
        M[M==0] = -1
        return M
        

    elif opt =='c':
        #Cold Start/ Equilibrium Start
        M = np.full((n, n), 1, dtype=int) #nxn Lattice with all +1
        return M

if __name__=="__main__":

     n = 10 #Lattice dimension
     T = 17.0 #Temperature
     N = 1000 #Number of iterations of Monte Carlo step
     opt = 'c' #starting condition, change between c & h for different starting condition

     M = init_spin_condition(opt) #Initial spin configuration

     #Simulating Ising Model
     fig = plt.figure(figsize=(13, 13), dpi=80)
     fig.suptitle("T = %0.01f" % T, fontsize=50)
     plt.xlim(0,n-1)
     plt.ylim(0,n-1)
     X, Y = np.meshgrid(range(n), range(n))
     mesh = plt.pcolormesh(X, Y, M, cmap = plt.cm.RdBu, vmin=-1, vmax=1)
     a = animation.FuncAnimation(fig, metrop, frames = N, interval = 10, blit = True)
     plt.show()
